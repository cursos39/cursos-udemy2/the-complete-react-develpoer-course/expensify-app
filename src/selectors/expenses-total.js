export default expenses =>
  expenses.reduce((totalAmount, expense) => totalAmount + expense.amount, 0)
