// SET_TEXT_FILTER
const setTextFilter = (filterName = '') => ({
  type: 'SET_TEXT_FILTER',
  text: filterName
})
// SORT_BY_DATE
const sortByDate = () => generateSortBy('date')

// SORT_BY_AMOUNT
const sortByAmount = () => generateSortBy('amount')

const generateSortBy = (sortBy) => ({
  type: 'SET_SORT_BY',
  sortBy
})

// SET_START_DATE
const setStartDate = (startDate) => ({
  type: 'SET_START_DATE',
  startDate
})
// SET_END_DATE
const setEndDate = (endDate) => ({
  type: 'SET_END_DATE',
  endDate
})

export { setTextFilter, sortByDate, sortByAmount, setStartDate, setEndDate, generateSortBy }
