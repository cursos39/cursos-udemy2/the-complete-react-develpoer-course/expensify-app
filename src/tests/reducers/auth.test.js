import authReducer from '../../reducers/auth.js'

test('should include user uid in the state', () => {
  const uid = '12345'
  const action = {
    type: 'LOGIN',
    uid
  }

  const state = authReducer(undefined, action)

  expect(state).toEqual({
    uid
  })
})

test('should remove user from state', () => {
  const uid = '12345'
  const action = { type: 'LOGOUT' }

  const state = authReducer({ uid }, action)

  expect(state).toEqual({})
})

test('should not do nothing with the state', () => {
  const uid = '12345'
  const action = { type: 'NOT_AN_ACTION' }

  const state = authReducer({ uid }, action)

  expect(state).toEqual({ uid })
})
