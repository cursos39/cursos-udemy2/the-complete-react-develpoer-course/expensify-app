import expensesReducer from '../../reducers/expenses'
import expenses from '../fixtures/expenses'

test('should generate default state', () => {
  const state = expensesReducer(undefined, { type: '@@INIT' })

  expect(state).toEqual([])
})

test('should remove expense by id', () => {
  const action = {
    type: 'REMOVE_EXPENSE',
    expenseId: expenses[1].id
  }

  const state = expensesReducer(expenses, action)

  expect(state).toEqual([expenses[0], expenses[2]])
})

test('should not remove expense if id not found', () => {
  const action = {
    type: 'REMOVE_EXPENSE',
    expenseId: 'not_found_id'
  }

  const state = expensesReducer(expenses, action)

  expect(state).toEqual([expenses[0], expenses[1], expenses[2]])
})

test('should add new expense', () => {
  const newExpense = {
    id: '4',
    description: 'Test expense',
    note: 'Expense from testing',
    createdAt: 123,
    amount: 200
  }
  const action = {
    type: 'ADD_EXPENSE',
    expense: newExpense
  }

  const state = expensesReducer(expenses, action)

  expect(state).toEqual([...expenses, newExpense])
})

test('should edit expense by id', () => {
  const expenseEdited = {
    note: 'note from testing'
  }
  const action = {
    type: 'EDIT_EXPENSE',
    expenseId: expenses[1].id,
    updates: expenseEdited
  }

  const state = expensesReducer(expenses, action)

  expect(state).toEqual([expenses[0], { ...expenses[1], ...expenseEdited }, expenses[2]])
})

test('should not edit expense if id not found', () => {
  const expenseEdited = {
    note: 'note from testing'
  }
  const action = {
    type: 'EDIT_EXPENSE',
    expenseId: 'not_found_id',
    updates: expenseEdited
  }

  const state = expensesReducer(expenses, action)

  expect(state).toEqual(expenses)
})

test('should set expenses', () => {
  const prevExpenses = [{ id: '1', note: 'expense1' }, { id: '2', note: 'expense1' }]
  const action = {
    type: 'SET_EXPENSES',
    expenses: [expenses[0], expenses[2]]
  }

  const state = expensesReducer(prevExpenses, action)

  expect(state).toEqual([expenses[0], expenses[2]])
})
