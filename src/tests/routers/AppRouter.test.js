import React from 'react'
import AppRouter from '../../routers/AppRouter'
import { shallow } from 'enzyme'

test('should render AppRouter correctly', () => {
  const wrapper = shallow(<AppRouter />)
  expect(wrapper).toMatchSnapshot()
})
