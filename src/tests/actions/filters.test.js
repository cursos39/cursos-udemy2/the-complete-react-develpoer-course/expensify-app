import {
  setTextFilter,
  sortByDate,
  sortByAmount,
  setStartDate,
  setEndDate
} from '../../actions/filters'
import moment from 'moment'

test('should generate correct default text filter', () => {
  const filter = setTextFilter()

  expect(filter).toEqual({
    type: 'SET_TEXT_FILTER',
    text: ''
  })
})

test('should generate correct new text filter', () => {
  const filter = setTextFilter('rent')

  expect(filter).toEqual({
    type: 'SET_TEXT_FILTER',
    text: 'rent'
  })
})

test('should generate set start date action object', () => {
  const action = setStartDate(moment(0))

  expect(action).toEqual({
    type: 'SET_START_DATE',
    startDate: moment(0)
  })
})

test('should generate set end date action object', () => {
  const action = setEndDate(moment(0))

  expect(action).toEqual({
    type: 'SET_END_DATE',
    endDate: moment(0)
  })
})

test('should generate sort by date action object', () => {
  const action = sortByDate()

  expect(action).toEqual({
    type: 'SET_SORT_BY',
    sortBy: 'date'
  })
})

test('should generate sort by amount action object', () => {
  const action = sortByAmount()

  expect(action).toEqual({
    type: 'SET_SORT_BY',
    sortBy: 'amount'
  })
})
