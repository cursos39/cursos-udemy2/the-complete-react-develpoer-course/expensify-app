import {
  addExpense,
  editExpense,
  removeExpense,
  setExpenses,
  startAddExpense,
  startSetExpenses,
  startRemoveExpense,
  startEditExpense
} from '../../actions/expenses.js'
import expenses from '../fixtures/expenses'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import db, { app } from '../../firebase/firebase.js'
import { get, ref, set } from 'firebase/database'
import { deleteApp } from 'firebase/app'

test('should setup remove expense action object', () => {
  const action = removeExpense({ id: '123abc' })

  expect(action).toEqual({
    type: 'REMOVE_EXPENSE',
    expenseId: '123abc'
  })
})

test('should setup edit expense action object', () => {
  const action = editExpense('1234abc', { note: 'New note value' })

  expect(action).toEqual({
    type: 'EDIT_EXPENSE',
    expenseId: '1234abc',
    updates: { note: 'New note value' }
  })
})

test('should setup add expense action object with provided values', () => {
  const action = addExpense(expenses[2])

  expect(action).toEqual({
    type: 'ADD_EXPENSE',
    expense: expenses[2]
  })
})

test('should setup set expense action object with data', () => {
  const action = setExpenses(expenses)

  expect(action).toEqual({
    type: 'SET_EXPENSES',
    expenses
  })
})

describe('Async tests', () => {
  const createMockStore = configureMockStore([thunk])
  const uid = 'test-uid'
  const defaultAuthState = { auth: { uid } }
  beforeEach(() => {
    const expensesData = {}
    expenses.forEach(({ id, description, note, amount, createdAt }) => {
      expensesData[id] = { description, note, amount, createdAt }
    })
    return set(ref(db, `users/${uid}/expenses`), expensesData)
  })
  afterAll(() => {
    deleteApp(app)
  })
  test('should add expense to database and store', async () => {
    const store = createMockStore(defaultAuthState)
    const expenseData = {
      description: 'Mouse',
      amount: 3000,
      note: 'This one is better',
      createdAt: 1000
    }

    return store.dispatch(startAddExpense(expenseData))
      .then(() => {
        const actions = store.getActions()
        expect(actions[0]).toEqual({
          type: 'ADD_EXPENSE',
          expense: {
            id: expect.any(String),
            ...expenseData
          }
        })
        return actions[0].expense
      }).then(expense => {
        return get(ref(db, `users/${uid}/expenses/${expense.id}`))
      }).then(snapshot => {
        return expect(snapshot.val()).toEqual(expenseData)
      })
  })
  test('should add expense with defaults to database and store', async () => {
    const store = createMockStore(defaultAuthState)
    const expenseDefault = {
      description: '',
      amount: 0,
      note: '',
      createdAt: 0
    }

    return store.dispatch(startAddExpense())
      .then(() => {
        const actions = store.getActions()
        expect(actions[0]).toEqual({
          type: 'ADD_EXPENSE',
          expense: {
            id: expect.any(String),
            ...expenseDefault
          }
        })
        return actions[0].expense
      }).then(expense => {
        return get(ref(db, `users/${uid}/expenses/${expense.id}`))
      }).then(snapshot => {
        return expect(snapshot.val()).toEqual(expenseDefault)
      })
  })
  test('should fetch the expenses from firebase', () => {
    const store = createMockStore(defaultAuthState)

    return store
      .dispatch(startSetExpenses())
      .then(() => {
        const actions = store.getActions()
        expect(actions).toEqual([{
          type: 'SET_EXPENSES',
          expenses
        }])
      })
  })

  test('Should remove expense from Firebase', () => {
    const store = createMockStore(defaultAuthState)

    return store
      .dispatch(startRemoveExpense({ id: expenses[1].id }))
      .then(() => {
        const actions = store.getActions()
        expect(actions).toEqual([{
          type: 'REMOVE_EXPENSE',
          expenseId: expenses[1].id
        }])
        return get(ref(db, `users/${uid}/expenses/${expenses[1].id}`))
      })
      .then(snapshot => {
        expect(snapshot.val()).toBeFalsy()
      })
  })

  test('should edit expense from Firebase', () => {
    const store = createMockStore(defaultAuthState)
    const { description, note, amount, createdAt } = expenses[0]
    const expenses0 = { description, note, amount, createdAt }

    return store
      .dispatch(startEditExpense(expenses[0].id, { note: 'Note from edit test' }))
      .then(() => {
        const actions = store.getActions()
        expect(actions).toEqual([{
          type: 'EDIT_EXPENSE',
          expenseId: expenses[0].id,
          updates: { note: 'Note from edit test' }
        }])
        return get(ref(db, `users/${uid}/expenses/${expenses[0].id}`))
      })
      .then(snapshot => {
        expect(snapshot.val()).toEqual({
          ...expenses0,
          note: 'Note from edit test'
        })
      })
  })
})
