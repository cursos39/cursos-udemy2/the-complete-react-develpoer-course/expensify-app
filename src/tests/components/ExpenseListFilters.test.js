import { shallow } from 'enzyme'
import moment from 'moment'
import React from 'react'
import { ExpenseListFilters } from '../../components/ExpenseListFilters'
import { altFilters, filters } from '../fixtures/filters'

describe('tests for the unit component ExpenseListFilters', () => {
  let setTextFilter, sortBy, setStartDate, setEndDate, wrapper

  beforeEach(() => {
    setTextFilter = jest.fn()
    sortBy = jest.fn()
    setStartDate = jest.fn()
    setEndDate = jest.fn()
    wrapper = shallow(
    <ExpenseListFilters
      filters={filters}
      setTextFilter={setTextFilter}
      generateSortBy={sortBy}
      setStartDate={setStartDate}
      setEndDate={setEndDate}
    />
    )
  })

  test('should render ExpenseListFilters correctly', () => {
    expect(wrapper).toMatchSnapshot()
  })

  test('should render ExpenseListFilters with alt data correctly', () => {
    wrapper.setProps({ filters: altFilters })

    expect(wrapper).toMatchSnapshot()
  })

  test('should handle text change', () => {
    const value = 'new text'
    wrapper.find('input').simulate('change', {
      target: {
        value
      }
    })

    expect(setTextFilter).toHaveBeenCalledWith(value)
  })

  test('should sort by date', () => {
    const value = 'date'

    wrapper.find('select').simulate('change', {
      target: { value }
    })

    expect(sortBy).toHaveBeenCalledWith(value)
  })

  test('should sort by amount', () => {
    const value = 'amount'

    wrapper.find('select').simulate('change', {
      target: { value }
    })

    expect(sortBy).toHaveBeenCalledWith(value)
  })

  test('should handle date changes', () => {
    const startDate = moment(0)
    const endDate = moment(0).add(4, 'days')
    wrapper.find('DateRangePicker').prop('onDatesChange')({ startDate, endDate })

    expect(setStartDate).toHaveBeenCalledWith(startDate)
    expect(setEndDate).toHaveBeenCalledWith(endDate)
  })

  test('should handle focus changes', () => {
    wrapper.find('DateRangePicker').prop('onFocusChange')('startDate')

    expect(wrapper.state('calendarFocused')).toBe('startDate')
  })
})
