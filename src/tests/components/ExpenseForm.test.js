import React from 'react'
import { shallow } from 'enzyme'
import ExpenseForm from '../../components/ExpenseForm'
import expenses from '../fixtures/expenses'
import moment from 'moment'

test('should render ExpenseForm with default expense correctly', () => {
  const wrapper = shallow(<ExpenseForm />)

  expect(wrapper).toMatchSnapshot()
})

test('should render ExpenseForm with expense data', () => {
  const wrapper = shallow(<ExpenseForm expense={expenses[1]}/>)

  expect(wrapper).toMatchSnapshot()
})

describe('tests with default expense passed', () => {
  let wrapper
  beforeEach(() => {
    wrapper = shallow(<ExpenseForm />)
  })
  test('should render error for invalid form submission', () => {
    expect(wrapper).toMatchSnapshot()
    wrapper.find('form').simulate('submit', {
      preventDefault: () => {}
    })
    expect(wrapper.state('errorState').length).toBeGreaterThan(0)
    expect(wrapper).toMatchSnapshot()
  })

  test('should set description on input change', () => {
    wrapper.find('input').at(0).simulate('change', {
      target: {
        value: 'Example description'
      }
    })

    expect(wrapper.state('description')).toBe('Example description')
  })

  test('should set note on textarea change', () => {
    wrapper.find('textarea').simulate('change', {
      target: {
        value: 'Example note'
      },
      persist: () => {}
    })

    expect(wrapper.state('note')).toBe('Example note')
  })

  // two test cases for amount --> one valid and one invalid
  test('should set amount if valid input', () => {
    wrapper.find('input').at(1).simulate('change', {
      target: { value: '23.50' }
    })

    expect(wrapper.state('amount')).toBe('23.50')
  })
  test('should not  set amount if invalid input', () => {
    wrapper.find('input').at(1).simulate('change', {
      target: { value: '12.122' }
    })

    expect(wrapper.state('amount')).toBe('')
  })
  test('should set new date on date change', () => {
    const newDate = moment()

    wrapper.find('SingleDatePicker').prop('onDateChange')(newDate)

    expect(wrapper.state('createdAt')).toEqual(newDate)
  })

  test('should change the calendar focused', () => {
    wrapper.find('SingleDatePicker').prop('onFocusChange')({ focused: true })

    expect(wrapper.state('calendarFocused')).toBe(true)
  })
})

test('Should call onSubmit prop for valid form submission', () => {
  const onSubmitSpy = jest.fn()

  const wrapper = shallow(<ExpenseForm expense={expenses[0]} onSubmit={onSubmitSpy}/>)

  wrapper.find('form').simulate('submit', {
    preventDefault: () => {}
  })

  expect(wrapper.state('errorState')).toBe('')
  expect(onSubmitSpy).toHaveBeenLastCalledWith(expenses[0])
})
