import { shallow } from 'enzyme'
import React from 'react'
import { addExpense } from '../../actions/expenses'
import EditExpensePageConnected, { EditExpensePage } from '../../components/EditExpensePage'
import expenses from '../fixtures/expenses'
import configureStore from '../../store/configureStore'

test('should render correctly', () => {
  const wrapper = shallow(<EditExpensePage match={{ params: { expenseId: 1 } }}/>)

  expect(wrapper).toMatchSnapshot()
})

describe('Tests with spies in EditExpensePage', () => {
  let history, editExpense, wrapper, removeExpense
  beforeEach(() => {
    history = {
      push: jest.fn()
    }
    editExpense = jest.fn()
    removeExpense = jest.fn()
    wrapper = shallow(
    <EditExpensePage
      match={{ params: { expenseId: expenses[0] } }}
      history={history}
      expense={expenses[0]}
      startEditExpense={editExpense}
      startRemoveExpense={removeExpense}
    />
    )
  })
  test('should handle editExpense', () => {
    wrapper.find('ExpenseForm').prop('onSubmit')(expenses[0])

    expect(history.push).toHaveBeenCalledWith('/')
    expect(editExpense).toHaveBeenCalledWith(expenses[0].id, expenses[0])
  })

  test('should handle removeExpense', () => {
    wrapper.find('button').simulate('click', undefined)

    expect(history.push).toHaveBeenCalledWith('/')
    expect(removeExpense).toHaveBeenCalledWith({ id: expenses[0].id })
  })
})

describe('connected component', () => {
  test('should render connected EditExpensePage correctly', () => {
    const store = configureStore()
    store.dispatch(addExpense(expenses[0]))
    store.dispatch(addExpense(expenses[1]))
    store.dispatch(addExpense(expenses[2]))

    const wrapper = shallow(<EditExpensePageConnected match={{ params: { expenseId: expenses[0] } }} store={store}/>)

    expect(wrapper).toMatchSnapshot()
  })
})
