import { shallow } from 'enzyme'
import ExpenseListItem from '../../components/ExpenseListItem'
import React from 'react'
import expenses from '../fixtures/expenses'

test('should render ExpenseListItem with all props informed', () => {
  const wrapper = shallow(<ExpenseListItem description='Rent' amount={1300} createdAt={1000} id={1234}/>)

  expect(wrapper).toMatchSnapshot()
})

test('should render ExpenseListItem with empty props', () => {
  const wrapper = shallow(<ExpenseListItem />)

  expect(wrapper).toMatchSnapshot()
})

test('should render ExpenseListItem with all props informed from expense 0', () => {
  const wrapper = shallow(<ExpenseListItem {...expenses[0]}/>)

  expect(wrapper).toMatchSnapshot()
})
