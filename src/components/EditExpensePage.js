import React from 'react'
import { connect } from 'react-redux'
import { startEditExpense, startRemoveExpense } from '../actions/expenses'
import ExpenseForm from './ExpenseForm'

export class EditExpensePage extends React.Component {
  redirectTo = to => this.props.history.push(to)
  redirectToHome = () => this.redirectTo('/')
  onClickRemove = () => {
    this.props.startRemoveExpense({ id: this.props.expense.id })
    this.redirectToHome()
  }

  onSubmitEditExpense = (expense) => {
    this.props.startEditExpense(this.props.expense.id, expense)
    this.redirectToHome()
  }

  render () {
    return (
    <div>
      <div className='page-header'>
        <div className='conten-container'>
          <h1 className='page-header__title'>Edit Expense</h1>
        </div>
      </div>
      <div className='content-container'>
        <ExpenseForm
          expense={this.props.expense}
          onSubmit={this.onSubmitEditExpense}
        />
        <button className='button button--secondary' onClick={this.onClickRemove}>Remove Expense</button>
      </div>
    </div>
    )
  }
}

const mapStateToProps = (state, props) => ({
  expense: state.expenses.find(expense => expense.id === props.match.params.expenseId)
})

const mapDispatchToProps = (dispatch) => ({
  startEditExpense: (expenseId, expense) => dispatch(startEditExpense(expenseId, expense)),
  startRemoveExpense: (expense) => dispatch(startRemoveExpense(expense))
})

export default connect(mapStateToProps, mapDispatchToProps)(EditExpensePage)
