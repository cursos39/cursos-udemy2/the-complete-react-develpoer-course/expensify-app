import React from 'react'
import { connect } from 'react-redux'
import getExpensesTotal from './../selectors/expenses-total.js'
import getVisibleExpenses from './../selectors/expenses.js'
import numeral from 'numeral'
import { Link } from 'react-router-dom'

export const ExpensesSummary = (props) => {
  const expenseWord = props.expenses.length === 1 ? 'expense' : 'expenses'
  const formatedAmount = numeral(props.amount / 100).format('0,0.00$')
  return (
  <div className='page-header'>
    <div className='content-container'>
      <h1 className='page-header__title'>
        Viewing <span>{props.expenses.length}</span> {expenseWord} totalling <span>{formatedAmount}</span>
      </h1>
      <div className='page-header__actions'>
        <Link className='button' to="/create">Add Expense</Link>
      </div>
    </div>
  </div>
  )
}

const mapStateToProps = (state) => {
  const expenses = getVisibleExpenses(state.expenses, state.filters)
  return {
    expenses,
    amount: getExpensesTotal(expenses)
  }
}

export default connect(mapStateToProps)(ExpensesSummary)
